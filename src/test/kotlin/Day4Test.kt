import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class Day4Test {

    private lateinit var data: List<String>
    private lateinit var day4: Day4

    @BeforeEach
    fun setup() {
        data = object {}.javaClass.getResourceAsStream("day4-test.txt")?.bufferedReader()?.readLines() ?: emptyList()
        day4 = Day4()
    }

    @Test
    fun `Should count all cards points`() {
        val points = day4.runPart1(data)
        assertThat(points).isEqualTo(13)
    }

    @Test
    fun `Should count card score with no match`() {
        val card = Day4.Card("Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36")
        assertThat(card.score).isEqualTo(0)
    }

    @Test
    fun `Should count card score with one item matching`() {
        val card = Day4.Card("Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83")
        assertThat(card.score).isEqualTo(1)
    }

    @Test
    fun `Should count card score with multiple items matching`() {
        val card = Day4.Card("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53")
        assertThat(card.score).isEqualTo(8)
    }

    @ParameterizedTest
    @MethodSource("provideCards")
    fun `Should count card score`(cardValue: String, expectedScore: Int) {
        val card = Day4.Card(cardValue)
        assertThat(card.score).isEqualTo(expectedScore)
    }

    @Test
    fun `Should count number of sratchcards`() {
        val cardCount = day4.runPart2(data)
        assertThat(cardCount).isEqualTo(30)
    }

    @Test
    fun `Should return index of card`() {
        val card = Day4.Card("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53")
        assertThat(card.index).isEqualTo(1)
    }

    @Test
    fun `Should return matching numbers of card`() {
        val card = Day4.Card("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53")
        assertThat(card.matchingNumbers).isEqualTo(4)
    }

    companion object {
        @JvmStatic
        private fun provideCards() = Stream.of(
            Arguments.of("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53", 8),
            Arguments.of("Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19", 2),
            Arguments.of("Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1", 2),
            Arguments.of("Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83", 1),
            Arguments.of("Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36", 0),
            Arguments.of("Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11", 0),
        )
    }
}
