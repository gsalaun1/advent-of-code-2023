
import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isFalse
import assertk.assertions.isTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class Day2Test {

    private lateinit var data: List<String>
    private lateinit var day2: Day2

    @BeforeEach
    fun setup() {
        data = object {}.javaClass.getResourceAsStream("day2-test.txt")?.bufferedReader()?.readLines() ?: emptyList()
        day2 = Day2()
    }

    @Test
    fun `Should sum possible games`() {
        val result = day2.runPart1(data)
        assertThat(result).isEqualTo(8)
    }

    @Test
    fun `Game with cubes should not exceed cubes collection`() {
        val game = Day2.Game("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green")
        assertThat(game.cubesCollectionDoesExceed(Day2.CubesCollection(12, 13, 14))).isFalse()
    }

    @Test
    fun `Game with cubes should exceed cubes collection`() {
        val game = Day2.Game("Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red")
        assertThat(game.cubesCollectionDoesExceed(Day2.CubesCollection(12, 13, 14))).isTrue()
    }

    @Test
    fun `Should build game maximum required cubes for one set`() {
        val game = Day2.Game("Game 3: 8 green, 6 blue, 20 red")
        assertThat(game.maximumRequiredCubesToMatch).isEqualTo(Day2.CubesCollection(20, 8, 6))
    }

    @Test
    fun `Should build game maximum required cubes for multiple sets`() {
        val game = Day2.Game("Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red")
        assertThat(game.maximumRequiredCubesToMatch).isEqualTo(Day2.CubesCollection(20, 13, 6))
    }

    @Test
    fun `Should return game id`() {
        val game = Day2.Game("Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red")
        assertThat(game.id).isEqualTo(3)
    }

    @Test
    fun `Should sum least possible games`() {
        val result = day2.runPart2(data)
        assertThat(result).isEqualTo(2286)
    }
}
