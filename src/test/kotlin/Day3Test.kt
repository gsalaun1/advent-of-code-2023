import assertk.assertThat
import assertk.assertions.containsExactly
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class Day3Test {

    private lateinit var data: List<String>
    private lateinit var day3: Day3

    @BeforeEach
    fun setup() {
        data = object {}.javaClass.getResourceAsStream("day3-test.txt")?.bufferedReader()?.readLines() ?: emptyList()
        day3 = Day3()
    }

    @Test
    fun `Should sum numbers adjacent to a symbol`() {
        val result = day3.runPart1(data)
        assertThat(result).isEqualTo(4361)
    }

    @Test
    fun `Should extract numbers from a single number`() {
        val extractedNumbers = day3.extractNumbers("467", 0)
        assertThat(extractedNumbers).containsExactly(Day3.LocatedNumber(467, 0, 2, 0))
    }

    @Test
    fun `Should extract numbers from a numbers and other symbols`() {
        val extractedNumbers = day3.extractNumbers("467..114..", 0)
        assertThat(extractedNumbers).containsExactly(
            Day3.LocatedNumber(467, 0, 2, 0),
            Day3.LocatedNumber(114, 5, 7, 0),
        )
    }

    @Test
    fun `Should extract symbols location`() {
        val extractedSymbols = day3.extractSymbolsLocation("...\$.*....", 0)
        assertThat(extractedSymbols).containsExactly(
            Day3.Location(3, 0),
            Day3.Location(5, 0),
        )
    }

    @Test
    fun `Should sum gear ratios`() {
        val result = day3.runPart2(data)
        assertThat(result).isEqualTo(467835)
    }

    @Test
    fun `Should extract gears location`() {
        val extractedGears = day3.extractGearsLocation("...\$.*....", 0)
        assertThat(extractedGears).containsExactly(
            Day3.Location(5, 0),
        )
    }
}
