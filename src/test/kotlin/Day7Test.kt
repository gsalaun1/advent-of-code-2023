import assertk.assertThat
import assertk.assertions.containsExactly
import assertk.assertions.isEqualTo
import assertk.assertions.isGreaterThan
import assertk.assertions.isLessThan
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.junit.jupiter.params.provider.ValueSource
import java.util.stream.Stream
import kotlin.random.Random

class Day7Test {

    private lateinit var data: List<String>
    private lateinit var day7: Day7

    @BeforeEach
    fun setup() {
        data = object {}.javaClass.getResourceAsStream("day7-test.txt")?.bufferedReader()?.readLines() ?: emptyList()
        day7 = Day7()
    }

    @Test
    fun `Should compute total winnings`() {
        val totalWinnings = day7.runPart1(data)
        assertThat(totalWinnings).isEqualTo(6440)
    }

    @Test
    fun `Should compute total winnings with real data`() {
        data = object {}.javaClass.getResourceAsStream("day7.txt")?.bufferedReader()?.readLines() ?: emptyList()
        val totalWinnings = day7.runPart1(data)
        assertThat(totalWinnings).isEqualTo(252656917)
    }

    @Test
    fun `Should compare two hands based on their types`() {
        val hand1 = Day7.ClassicalHand.random("AAAAA")
        val hand2 = Day7.ClassicalHand.random("23456")
        assertThat(hand1.compareTo(hand2)).isGreaterThan(0)
    }

    @ParameterizedTest
    @MethodSource("provideHandValues")
    fun `Should find type for hand value`(value: String, handType: Day7.HandType) {
        val hand = Day7.ClassicalHand.random(value)
        assertThat(hand.type).isEqualTo(handType)
    }

    companion object {
        @JvmStatic
        private fun provideHandValues() = Stream.of(
            Arguments.of("AAAAA", Day7.HandType.FIVE_OF_A_KIND),
            Arguments.of("AA8AA", Day7.HandType.FOUR_OF_A_KIND),
            Arguments.of("23332", Day7.HandType.FULL_HOUSE),
            Arguments.of("TTT98", Day7.HandType.THREE_OF_A_KIND),
            Arguments.of("23432", Day7.HandType.TWO_PAIR),
            Arguments.of("A23A4", Day7.HandType.ONE_PAIR),
            Arguments.of("23456", Day7.HandType.HIGH_CARD),
        )
    }

    @Test
    fun `Should compare two hands based on their first char values`() {
        val hand1 = Day7.ClassicalHand.random("AAAAA")
        val hand2 = Day7.ClassicalHand.random("KKKKK")
        assertThat(hand1.compareTo(hand2)).isGreaterThan(0)
    }

    private fun Day7.ClassicalHand.Companion.random(value: String) = Day7.ClassicalHand(value, Random.nextInt())

    @Test
    fun `Should compute total winnings with joker as wild card`() {
        val totalWinnings = day7.runPart2(data)
        assertThat(totalWinnings).isEqualTo(5905)
    }

    @Test
    fun `Should build hand with one pair and no joker as wild card`() {
        val hand = Day7.HandWithJokerAsWildCard.random("32T3K")
        assertThat(hand.type).isEqualTo(Day7.HandType.ONE_PAIR)
    }

    @Test
    fun `Should build hand with two pairs and no joker as wild card`() {
        val hand = Day7.HandWithJokerAsWildCard.random("KK677")
        assertThat(hand.type).isEqualTo(Day7.HandType.TWO_PAIR)
    }

    @ParameterizedTest
    @ValueSource(strings = ["T55J5", "KTJJT", "QQQJA"])
    fun `Should build hand with four of a kind and joker as wild card`(value: String) {
        val hand = Day7.HandWithJokerAsWildCard.random(value)
        assertThat(hand.type).isEqualTo(Day7.HandType.FOUR_OF_A_KIND)
    }

    @Test
    fun `Should compute total winnings with joker as wild card with real data`() {
        data = object {}.javaClass.getResourceAsStream("day7.txt")?.bufferedReader()?.readLines() ?: emptyList()
        val totalWinnings = day7.runPart2(data)
        assertThat(totalWinnings).isEqualTo(253499763)
    }

    private fun Day7.HandWithJokerAsWildCard.Companion.random(value: String) =
        Day7.HandWithJokerAsWildCard(value, Random.nextInt())
}