import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class Day1Test {

    private lateinit var day1: Day1

    @BeforeEach
    fun setup() {
        day1 = Day1()
    }

    @Test
    fun `Should find Calibration value for a single line with numbers at start and end`() {
        assertThat(day1.runPart1(listOf("1abc2"))).isEqualTo(12)
    }

    @Test
    fun `Should find Calibration value for a single line with numbers anywhere`() {
        assertThat(day1.runPart1(listOf("pqr3stu8vwx"))).isEqualTo(38)
    }

    @Test
    fun `Should find Calibration value for data file`() {
        val data = object {}.javaClass.getResourceAsStream("day1-1-test.txt")?.bufferedReader()?.readLines() ?: emptyList()
        assertThat(day1.runPart1(data)).isEqualTo(142)
    }

    @Test
    fun `Should find Calibration value for single line with number spelled out with letters`() {
        assertThat(day1.runPart2(listOf("two1nine"))).isEqualTo(29)
    }

    @ParameterizedTest
    @MethodSource("provideNumbersSpelledOutWithIntermixedLetters")
    fun `Should find Calibration value for single line with numbers spelled out with intermixed letters`(
        input: String,
        expected: Int,
    ) {
        assertThat(day1.runPart2(listOf(input))).isEqualTo(expected)
    }

    @Test
    fun `Should find Calibration value for data file with number spelled out with letters`() {
        val data = object {}.javaClass.getResourceAsStream("day1-2-test.txt")?.bufferedReader()?.readLines() ?: emptyList()
        assertThat(day1.runPart2(data)).isEqualTo(281)
    }

    companion object {
        @JvmStatic
        fun provideNumbersSpelledOutWithIntermixedLetters() = Stream.of(
            Arguments.of("xtwone3four", 24),
            Arguments.of("oneight", 18),
            Arguments.of("fourxzhgjfrrbmkcheightfive7seven8oneightb", 48),
            Arguments.of("pcg91vqrfpxxzzzoneightzt", 98),
        )
    }
}
