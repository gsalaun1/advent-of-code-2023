import assertk.assertThat
import assertk.assertions.containsExactly
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class Day6Test {

    private lateinit var data: List<String>
    private lateinit var day6: Day6

    @BeforeEach
    fun setup() {
        data = object {}.javaClass.getResourceAsStream("day6-test.txt")?.bufferedReader()?.readLines() ?: emptyList()
        day6 = Day6()
    }

    @Test
    fun `Should multiply number of ways to beat the record`() {
        val numberOfWaysMultiplied = day6.runPart1(data)
        assertThat(numberOfWaysMultiplied).isEqualTo(288)
    }

    @Test
    fun `Should multiply number of ways to beat the record with real data`() {
        data = object {}.javaClass.getResourceAsStream("day6.txt")?.bufferedReader()?.readLines() ?: emptyList()
        val numberOfWaysMultiplied = day6.runPart1(data)
        assertThat(numberOfWaysMultiplied).isEqualTo(2756160)
    }

    @Test
    fun `Should build races`() {
        val races = Day6.Races(data)
        assertThat(races.races).containsExactly(
            Day6.Race(7, 9),
            Day6.Race(15, 40),
            Day6.Race(30, 200)
        )
    }

    @ParameterizedTest
    @MethodSource("provideRacesAndNumberOfWaysToBeatRecord")
    fun `Should find number of ways to beat record`(time: Long, distance: Long, numberOfWaysToBeatRecord: Long) {
        val race = Day6.Race(time, distance)
        assertThat(race.numberOfWaysToBeatRecord).isEqualTo(numberOfWaysToBeatRecord)
    }

    companion object {
        @JvmStatic
        private fun provideRacesAndNumberOfWaysToBeatRecord() = Stream.of(
            Arguments.of(7, 9, 4),
            Arguments.of(15, 40, 8),
            Arguments.of(30, 200, 9),
        )
    }

    @Test
    fun `Should find number of ways to beat record when all data are concatenated`() {
        val numberOfWays = day6.runPart2(data)
        assertThat(numberOfWays).isEqualTo(71503)
    }

    @Test
    fun `Should instantiate Race from a list of lines`(){
        val race = Day6.Race.of(data)
        assertThat(race).isEqualTo(Day6.Race(71530,940200))
    }
}