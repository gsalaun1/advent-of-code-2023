import assertk.assertThat
import assertk.assertions.containsAll
import assertk.assertions.containsExactly
import assertk.assertions.containsExactlyInAnyOrder
import assertk.assertions.isEqualTo
import assertk.assertions.isFalse
import assertk.assertions.isGreaterThan
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class Day5Test {

    private lateinit var data: List<String>
    private lateinit var day5: Day5

    @BeforeEach
    fun setup() {
        data = object {}.javaClass.getResourceAsStream("day5-test.txt")?.bufferedReader()?.readLines() ?: emptyList()
        day5 = Day5()
    }

    @Test
    fun `Should find lowest location number`() {
        val lowestLocationNumber = day5.runPart1(data)
        assertThat(lowestLocationNumber).isEqualTo(35)
    }

    @Test
    fun `Should find lowest location number  with real data`() {
        data = object {}.javaClass.getResourceAsStream("day5.txt")?.bufferedReader()?.readLines() ?: emptyList()
        val lowestLocationNumber = day5.runPart1(data)
        assertThat(lowestLocationNumber).isEqualTo(324724204)
    }

    @Nested
    inner class SingleSeedAlmanacTest {

        @Test
        fun `Should return seeds correctly affected to soil`() {
            val testData = listOf(
                "seeds: 79 14 55 13",
                "",
                "seed-to-soil map:",
                "50 98 2",
                "52 50 48",
            )
            val almanac = Day5.SingleSeedAlmanac(testData)
            assertThat(almanac.seeds).containsExactly(
                Day5.Seed(79, 81),
                Day5.Seed(14, 14),
                Day5.Seed(55, 57),
                Day5.Seed(13, 13),
            )
        }

        @Test
        fun `Should return seeds correctly affected to soil and fertilizer`() {
            val testData = listOf(
                "seeds: 79 14 55 13",
                "",
                "seed-to-soil map:",
                "50 98 2",
                "52 50 48",
                "",
                "soil-to-fertilizer map:",
                "0 15 37",
                "37 52 2",
                "39 0 15",
            )
            val almanac = Day5.SingleSeedAlmanac(testData)
            assertThat(almanac.seeds).containsExactly(
                Day5.Seed(79, 81, 81),
                Day5.Seed(14, 14, 53),
                Day5.Seed(55, 57, 57),
                Day5.Seed(13, 13, 52),
            )
        }

        @Test
        fun `Should return seeds correctly affected to soil, fertilizer and water`() {
            val testData = listOf(
                "seeds: 79 14 55 13",
                "",
                "seed-to-soil map:",
                "50 98 2",
                "52 50 48",
                "",
                "soil-to-fertilizer map:",
                "0 15 37",
                "37 52 2",
                "39 0 15",
                "",
                "fertilizer-to-water map:",
                "49 53 8",
                "0 11 42",
                "42 0 7",
                "57 7 4",
            )
            val almanac = Day5.SingleSeedAlmanac(testData)
            assertThat(almanac.seeds).containsExactly(
                Day5.Seed(79, 81, 81, 81),
                Day5.Seed(14, 14, 53, 49),
                Day5.Seed(55, 57, 57, 53),
                Day5.Seed(13, 13, 52, 41),
            )
        }

        @Test
        fun `Should return seeds correctly affected to soil, fertilizer, water and light`() {
            val testData = listOf(
                "seeds: 79 14 55 13",
                "",
                "seed-to-soil map:",
                "50 98 2",
                "52 50 48",
                "",
                "soil-to-fertilizer map:",
                "0 15 37",
                "37 52 2",
                "39 0 15",
                "",
                "fertilizer-to-water map:",
                "49 53 8",
                "0 11 42",
                "42 0 7",
                "57 7 4",
                "",
                "water-to-light map:",
                "88 18 7",
                "18 25 70",
            )
            val almanac = Day5.SingleSeedAlmanac(testData)
            assertThat(almanac.seeds).containsExactly(
                Day5.Seed(79, 81, 81, 81, 74),
                Day5.Seed(14, 14, 53, 49, 42),
                Day5.Seed(55, 57, 57, 53, 46),
                Day5.Seed(13, 13, 52, 41, 34),
            )
        }

        @Test
        fun `Should return seeds correctly affected to soil, fertilizer, water, light and temperature`() {
            val testData = listOf(
                "seeds: 79 14 55 13",
                "",
                "seed-to-soil map:",
                "50 98 2",
                "52 50 48",
                "",
                "soil-to-fertilizer map:",
                "0 15 37",
                "37 52 2",
                "39 0 15",
                "",
                "fertilizer-to-water map:",
                "49 53 8",
                "0 11 42",
                "42 0 7",
                "57 7 4",
                "",
                "water-to-light map:",
                "88 18 7",
                "18 25 70",
                "",
                "light-to-temperature map:",
                "45 77 23",
                "81 45 19",
                "68 64 13",
            )
            val almanac = Day5.SingleSeedAlmanac(testData)
            assertThat(almanac.seeds).containsExactly(
                Day5.Seed(79, 81, 81, 81, 74, 78),
                Day5.Seed(14, 14, 53, 49, 42, 42),
                Day5.Seed(55, 57, 57, 53, 46, 82),
                Day5.Seed(13, 13, 52, 41, 34, 34),
            )
        }

        @Test
        fun `Should return seeds correctly affected to soil, fertilizer, water, light, temperature and humidity`() {
            val testData = listOf(
                "seeds: 79 14 55 13",
                "",
                "seed-to-soil map:",
                "50 98 2",
                "52 50 48",
                "",
                "soil-to-fertilizer map:",
                "0 15 37",
                "37 52 2",
                "39 0 15",
                "",
                "fertilizer-to-water map:",
                "49 53 8",
                "0 11 42",
                "42 0 7",
                "57 7 4",
                "",
                "water-to-light map:",
                "88 18 7",
                "18 25 70",
                "",
                "light-to-temperature map:",
                "45 77 23",
                "81 45 19",
                "68 64 13",
                "",
                "temperature-to-humidity map:",
                "0 69 1",
                "1 0 69",
            )
            val almanac = Day5.SingleSeedAlmanac(testData)
            assertThat(almanac.seeds).containsExactly(
                Day5.Seed(79, 81, 81, 81, 74, 78, 78),
                Day5.Seed(14, 14, 53, 49, 42, 42, 43),
                Day5.Seed(55, 57, 57, 53, 46, 82, 82),
                Day5.Seed(13, 13, 52, 41, 34, 34, 35),
            )
        }

        @Test
        fun `Should return seeds correctly affected to all correct values`() {
            val testData = listOf(
                "seeds: 79 14 55 13",
                "",
                "seed-to-soil map:",
                "50 98 2",
                "52 50 48",
                "",
                "soil-to-fertilizer map:",
                "0 15 37",
                "37 52 2",
                "39 0 15",
                "",
                "fertilizer-to-water map:",
                "49 53 8",
                "0 11 42",
                "42 0 7",
                "57 7 4",
                "",
                "water-to-light map:",
                "88 18 7",
                "18 25 70",
                "",
                "light-to-temperature map:",
                "45 77 23",
                "81 45 19",
                "68 64 13",
                "",
                "temperature-to-humidity map:",
                "0 69 1",
                "1 0 69",
                "",
                "humidity-to-location map:",
                "60 56 37",
                "56 93 4",
            )
            val almanac = Day5.SingleSeedAlmanac(testData)
            assertThat(almanac.seeds).containsExactly(
                Day5.Seed(79, 81, 81, 81, 74, 78, 78, 82),
                Day5.Seed(14, 14, 53, 49, 42, 42, 43, 43),
                Day5.Seed(55, 57, 57, 53, 46, 82, 82, 86),
                Day5.Seed(13, 13, 52, 41, 34, 34, 35, 35),
            )
        }
    }

    @Test
    fun `Should find lowest location number for seeds range`() {
        val lowestLocationNumber = day5.runPart2(data)
        assertThat(lowestLocationNumber).isEqualTo(46)
    }

    @Test
    fun `Should find lowest location number for seeds range  with real data`() {
        data = object {}.javaClass.getResourceAsStream("day5.txt")?.bufferedReader()?.readLines() ?: emptyList()
        val lowestLocationNumber = day5.runPart2(data)
        assertThat(lowestLocationNumber).isEqualTo(104070862)
    }

    @Nested
    inner class SeedsRangeAlmanacTest {

        @Test
        fun `Should init seeds ranges`() {
            val testData = listOf(
                "seeds: 79 14 55 13",
            )
            val almanac = Day5.SeedsRangeAlmanac(testData)
            assertThat(almanac.dataRanges).containsExactlyInAnyOrder(
                Day5.SeedsRangeAlmanac.DataRange(55, 67),
                Day5.SeedsRangeAlmanac.DataRange(79, 92),
            )
        }

        @Test
        fun `Should compute soils ranges when not intermixing with seeds ranges`() {
            val testData = listOf(
                "seeds: 79 14 55 13",
                "",
                "seed-to-soil map:",
                "50 98 2",
            )
            val almanac = Day5.SeedsRangeAlmanac(testData)
            assertThat(almanac.dataRanges).containsExactlyInAnyOrder(
                Day5.SeedsRangeAlmanac.DataRange(55, 67),
                Day5.SeedsRangeAlmanac.DataRange(79, 92),
            )
        }

        @Test
        fun `Should compute soils ranges when overwhelming seeds ranges`() {
            val testData = listOf(
                "seeds: 79 14 55 13",
                "",
                "seed-to-soil map:",
                "50 98 2",
                "52 50 48",
            )
            val almanac = Day5.SeedsRangeAlmanac(testData)
            assertThat(almanac.dataRanges).containsExactlyInAnyOrder(
                Day5.SeedsRangeAlmanac.DataRange(57, 69),
                Day5.SeedsRangeAlmanac.DataRange(81, 94),
            )
        }

        @Test
        fun `Should compute soils ranges when map is overriding values and one line of soil`() {
            val testData = listOf(
                "seeds: 46 4 54 9 74 14",
                "",
                "seed-to-soil map:",
                "45 77 23",
            )
            val almanac = Day5.SeedsRangeAlmanac(testData)
            assertThat(almanac.dataRanges).containsExactlyInAnyOrder(
                Day5.SeedsRangeAlmanac.DataRange(46, 49),
                Day5.SeedsRangeAlmanac.DataRange(54, 62),
                Day5.SeedsRangeAlmanac.DataRange(74, 76),
                Day5.SeedsRangeAlmanac.DataRange(45, 55),
            )
        }

        @Test
        fun `Should compute soils ranges when map is overriding values and two lines of soil`() {
            val testData = listOf(
                "seeds: 46 4 54 9 74 14",
                "",
                "seed-to-soil map:",
                "45 77 23",
                "81 45 19"
            )
            val almanac = Day5.SeedsRangeAlmanac(testData)
            assertThat(almanac.dataRanges).containsExactlyInAnyOrder(
                Day5.SeedsRangeAlmanac.DataRange(82, 85),
                Day5.SeedsRangeAlmanac.DataRange(90, 98),
                Day5.SeedsRangeAlmanac.DataRange(74, 76),
                Day5.SeedsRangeAlmanac.DataRange(45, 55),
            )
        }

        @Test
        fun `Should compute soils ranges when map is overriding values`() {
            val testData = listOf(
                "seeds: 46 4 54 9 74 14",
                "",
                "seed-to-soil map:",
                "45 77 23",
                "81 45 19",
                "68 64 13",
            )
            val almanac = Day5.SeedsRangeAlmanac(testData)
            assertThat(almanac.dataRanges).containsExactlyInAnyOrder(
                Day5.SeedsRangeAlmanac.DataRange(82, 85),
                Day5.SeedsRangeAlmanac.DataRange(90, 98),
                Day5.SeedsRangeAlmanac.DataRange(78, 80),
                Day5.SeedsRangeAlmanac.DataRange(45, 55),
            )
        }

        @Nested
        inner class DataRangeTest {

            @Test
            fun `should return soil range with map rule applied when overwhelmed`() {
                val dataRange = Day5.SeedsRangeAlmanac.DataRange(55, 67)
                val appliedSoilRange = dataRange.applyMapRule(52, 50, 48)
                assertThat(appliedSoilRange).containsAll(Day5.SeedsRangeAlmanac.DataRange(57, 69))
            }

            @Test
            fun `should return soil range with map rule not applied if before`() {
                val dataRange = Day5.SeedsRangeAlmanac.DataRange(55, 67)
                val appliedSoilRange = dataRange.applyMapRule(52, 50, 1)
                assertThat(appliedSoilRange).containsAll(Day5.SeedsRangeAlmanac.DataRange(55, 67))
            }

            @Test
            fun `should return soil range with map rule applied if starting before start and ending as same end`() {
                val dataRange = Day5.SeedsRangeAlmanac.DataRange(55, 67)
                val appliedSoilRange = dataRange.applyMapRule(52, 50, 18)
                assertThat(appliedSoilRange).containsAll(Day5.SeedsRangeAlmanac.DataRange(57, 69))
            }

            @Test
            fun `should return soil range with map rule applied if starting before start and ending before end`() {
                val dataRange = Day5.SeedsRangeAlmanac.DataRange(55, 67)
                val appliedSoilRange = dataRange.applyMapRule(52, 50, 10)
                assertThat(appliedSoilRange).containsExactlyInAnyOrder(
                    Day5.SeedsRangeAlmanac.DataRange(57, 61),
                    Day5.SeedsRangeAlmanac.DataRange(60, 67),
                )
            }

            @Test
            fun `should return soil range with map rule applied if starting after start and ending as same end`() {
                val dataRange = Day5.SeedsRangeAlmanac.DataRange(55, 67)
                val appliedSoilRange = dataRange.applyMapRule(62, 60, 8)
                assertThat(appliedSoilRange).containsExactlyInAnyOrder(
                    Day5.SeedsRangeAlmanac.DataRange(55, 59),
                    Day5.SeedsRangeAlmanac.DataRange(62, 69),
                )
            }

            @Test
            fun `should return soil range with map rule applied if starting after start and ending before end`() {
                val dataRange = Day5.SeedsRangeAlmanac.DataRange(55, 67)
                val appliedSoilRange = dataRange.applyMapRule(62, 60, 5)
                assertThat(appliedSoilRange).containsExactlyInAnyOrder(
                    Day5.SeedsRangeAlmanac.DataRange(55, 59),
                    Day5.SeedsRangeAlmanac.DataRange(62, 66),
                    Day5.SeedsRangeAlmanac.DataRange(65, 67),
                )
            }
        }

        @Test
        fun `Should apply map rule for production values`() {
            val dataRange = Day5.SeedsRangeAlmanac.DataRange(2999858074, 3097845530)
            val resultedRanges = dataRange.applyMapRule(383981385, 3064707638, 71598349)
            assertThat(resultedRanges).containsExactly(
                Day5.SeedsRangeAlmanac.DataRange(2999858074, 3064707637),
                Day5.SeedsRangeAlmanac.DataRange(383981385, 417119277)
            )
        }

        @Test
        fun `Should apply map rule for simple data`() {
            val dataRange = Day5.SeedsRangeAlmanac.DataRange(74, 87)
            val resultingRanges = dataRange.applyMapRule(45, 77, 23)
            assertThat(resultingRanges).containsExactly(
                Day5.SeedsRangeAlmanac.DataRange(74, 76),
                Day5.SeedsRangeAlmanac.DataRange(45, 55),
            )
        }

        @Test
        fun `Should apply map rule for source + range -1 lower than end and source lower than destination`() {
            val dataRange = Day5.SeedsRangeAlmanac.DataRange(11, 22)
            val resultingRanges = dataRange.applyMapRule(29, 13, 3)
            assertThat(resultingRanges).containsExactly(
                Day5.SeedsRangeAlmanac.DataRange(11, 12),
                Day5.SeedsRangeAlmanac.DataRange(29, 31),
                Day5.SeedsRangeAlmanac.DataRange(16, 22),
            )
        }

        @Test
        fun `Should apply map rule for source + range -1 lower than end and source greater than destination`() {
            val dataRange = Day5.SeedsRangeAlmanac.DataRange(11, 22)
            val resultingRanges = dataRange.applyMapRule(4, 13, 3)
            assertThat(resultingRanges).containsExactly(
                Day5.SeedsRangeAlmanac.DataRange(11, 12),
                Day5.SeedsRangeAlmanac.DataRange(4, 6),
                Day5.SeedsRangeAlmanac.DataRange(16, 22),
            )
        }

        @Test
        fun `Should apply map rule for source + range -1 equal end and source lower than destination`() {
            val dataRange = Day5.SeedsRangeAlmanac.DataRange(11, 22)
            val resultingRanges = dataRange.applyMapRule(34, 13, 10)
            assertThat(resultingRanges).containsExactly(
                Day5.SeedsRangeAlmanac.DataRange(11, 12),
                Day5.SeedsRangeAlmanac.DataRange(34, 43)
            )
        }

        @Test
        fun `Should apply map rule for source + range -1 equal end and source greater than destination`() {
            val dataRange = Day5.SeedsRangeAlmanac.DataRange(11, 22)
            val resultingRanges = dataRange.applyMapRule(4, 13, 10)
            assertThat(resultingRanges).containsExactly(
                Day5.SeedsRangeAlmanac.DataRange(11, 12),
                Day5.SeedsRangeAlmanac.DataRange(4, 13)
            )
        }

        @Test
        fun `Should apply map rule when source plus range is contained in dataRange`() {
            val dataRange = Day5.SeedsRangeAlmanac.DataRange(90, 98)
            val resultingRanges = dataRange.applyMapRule(60, 56, 37)
            assertThat(resultingRanges).containsExactly(
                Day5.SeedsRangeAlmanac.DataRange(94, 96),
                Day5.SeedsRangeAlmanac.DataRange(93, 98)
            )
        }

        @Test
        fun `Should apply map rule when source greater than destination and source is equal to start`() {
            val dataRange = Day5.SeedsRangeAlmanac.DataRange(93, 98)
            val resultingRanges = dataRange.applyMapRule(56, 93, 4)
            assertThat(resultingRanges).containsExactly(
                Day5.SeedsRangeAlmanac.DataRange(56, 59)
            )
        }

        @Test
        fun `Should apply map rule when end greater than start + range and source greater than destination with production values`() {
            val dataRange = Day5.SeedsRangeAlmanac.DataRange(1572765384, 1640984362)
            val resultingRanges = dataRange.applyMapRule(920009866, 1634093064, 30433490)
            assertThat(resultingRanges).containsExactly(
                Day5.SeedsRangeAlmanac.DataRange(1572765384, 1634093063),
                Day5.SeedsRangeAlmanac.DataRange(920009866, 926901164)
            )
        }

        @Test
        fun `Should apply map rule when end greater than start + range and source greater than destination`() {
            val dataRange = Day5.SeedsRangeAlmanac.DataRange(6, 11)
            val resultingRanges = dataRange.applyMapRule(5, 10, 3)
            assertThat(resultingRanges).containsExactly(
                Day5.SeedsRangeAlmanac.DataRange(6, 9),
                Day5.SeedsRangeAlmanac.DataRange(5, 6)
            )
        }
    }
}
