import assertk.assertThat
import assertk.assertions.containsExactly
import assertk.assertions.isEqualTo
import assertk.assertions.isGreaterThan
import assertk.assertions.isLessThan
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.junit.jupiter.params.provider.ValueSource
import java.util.stream.Stream
import kotlin.random.Random

class Day8Test {

    private lateinit var data: List<String>
    private lateinit var day8: Day8

    @BeforeEach
    fun setup() {
        day8 = Day8()
    }

    @Test
    fun `Should count steps for test set with no loop necessary`() {
        data = object {}.javaClass.getResourceAsStream("day8-1-test.txt")?.bufferedReader()?.readLines() ?: emptyList()
        val steps = day8.runPart1(data)
        assertThat(steps).isEqualTo(2)
    }

    @Test
    fun `Should count steps for test set with loop necessary`() {
        data = object {}.javaClass.getResourceAsStream("day8-2-test.txt")?.bufferedReader()?.readLines() ?: emptyList()
        val steps = day8.runPart1(data)
        assertThat(steps).isEqualTo(6)
    }

    @Test
    fun `Should count steps with real data`() {
        data = object {}.javaClass.getResourceAsStream("day8.txt")?.bufferedReader()?.readLines() ?: emptyList()
        val steps = day8.runPart1(data)
        assertThat(steps).isEqualTo(16697)
    }

    @Nested
    inner class DesertMapTest {
        @Test
        fun `Should count step for one matching instruction at the left side`() {
            val desertMap = Day8.DesertMap(
                "L",
                mapOf(
                    Day8.Node("AAA") to Pair(Day8.Node("ZZZ"), Day8.Node("BBB")),
                    Day8.Node("ZZZ") to Pair(Day8.Node("ZZZ"), Day8.Node("ZZZ"))
                )
            )
            assertThat(desertMap.countSteps()).isEqualTo(1)
        }

        @Test
        fun `Should count step for one matching instruction at the right side`() {
            val desertMap = Day8.DesertMap(
                "R", mapOf(
                    Day8.Node("AAA") to Pair(Day8.Node("BBB"), Day8.Node("ZZZ")),
                    Day8.Node("ZZZ") to Pair(Day8.Node("ZZZ"), Day8.Node("ZZZ"))
                )
            )
            assertThat(desertMap.countSteps()).isEqualTo(1)
        }
    }
}