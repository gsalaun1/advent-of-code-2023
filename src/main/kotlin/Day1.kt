import java.io.InputStream

fun main() {
    val day1 = Day1()
    day1.run("day1.txt")?.let {
        println("Day 1 Part 1 : ${it.first.part1}")
        println("Day 1 Part 2 : ${it.first.part2}")
    }
}

class Day1 : Day<String, Int, Int>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>) =
        lines.map { computeLine(it, searchedIntegers) }.sum()

    override fun runPart2(lines: List<String>) =
        lines.map { computeLine(it, searchedIntegers + searchedLetters) }.sum()

    private fun computeLine(line: String, items: List<Pair<String, Int>>) =
        line.findFirstAndLastNumbers(items).let { numbers ->
            numbers.first * 10 + numbers.second
        }

    private fun String.findFirstAndLastNumbers(items: List<Pair<String, Int>>): Pair<Int, Int> {
        val sortedPositions =
            items
                .flatMap {
                    listOf(
                        Pair(this.indexOf(it.first), it.second),
                        Pair(this.lastIndexOf(it.first), it.second),
                    )
                }
                .filter { it.first > -1 }
                .sortedBy { it.first }
        return Pair(sortedPositions.first().second, sortedPositions.last().second)
    }

    companion object {
        val searchedIntegers = (1..9).map { Pair(it.toString(), it) }
        val searchedLetters =
            listOf(
                Pair("one", 1),
                Pair("two", 2),
                Pair("three", 3),
                Pair("four", 4),
                Pair("five", 5),
                Pair("six", 6),
                Pair("seven", 7),
                Pair("eight", 8),
                Pair("nine", 9),
            )
    }
}
