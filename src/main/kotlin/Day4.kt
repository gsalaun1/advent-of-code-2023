import java.io.InputStream
import kotlin.math.pow

fun main() {
    val day4 = Day4()
    day4.run("day4.txt")?.let {
        println("Day 4 Part 1 : ${it.first.part1}")
        println("Day 4 Part 2 : ${it.first.part2}")
    }
}

class Day4 : Day<String, Int, Int>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>) =
        lines.map { Card(it) }
            .sumOf { it.score }

    override fun runPart2(lines: List<String>) =
        lines.map { Card(it) }
            .fold(mutableMapOf<Int, Int>()) { acc, card ->
                acc[card.index] = (acc[card.index] ?: 0) + 1
                (1..(acc[card.index] ?: 1)).forEach {
                    (1..card.matchingNumbers).forEach {
                        acc[card.index + it] = (acc[card.index + it] ?: 0) + 1
                    }
                }
                acc
            }.values.sum()

    class Card(value: String) {
        val matchingNumbers: Int

        val index: Int

        val score: Int

        init {
            val (cardName, numbers) = value.split(":")
            index = cardName.split(" ").last().toInt()
            val (winningNumbers, cardNumbers) = numbers
                .split("|")
                .map {
                    it
                        .split(" ")
                        .filter { it.isNotEmpty() }
                        .map { it.trim() }
                }
            matchingNumbers =
                cardNumbers.intersect(winningNumbers).size
            score = if (matchingNumbers == 0) {
                0
            } else {
                2.0.pow(matchingNumbers.toDouble() - 1.toDouble()).toInt()
            }
        }
    }
}
