import java.io.InputStream
import java.util.SortedSet
import java.util.TreeSet

fun main() {
    val day8 = Day8()
    day8.run("day8.txt")?.let {
        println("Day 8 Part 1 : ${it.first.part1}")
        println("Day 8 Part 2 : ${it.first.part2}")
    }
}

class Day8 : Day<String, Int, Int>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>) =
        DesertMap.of(lines).countSteps()

    class DesertMap(val instructions: String, val nodes: Map<Node, Pair<Node, Node>>) {
        companion object {}

        fun countSteps(): Int {
            var instructionIndex = 0
            var currentNode = nodes.entries.first { it.key == Node("AAA") }
            var steps = 0
            while (currentNode.key != Node("ZZZ")) {
                val direction = instructions[instructionIndex]
                val targetNode = when (direction) {
                    'L' -> currentNode.value.first
                    'R' -> currentNode.value.second
                    else -> throw IllegalStateException("Direction not supported")
                }
                currentNode = nodes.filter { it.key == targetNode }.entries.first()
                instructionIndex = (instructionIndex + 1) % instructions.length
                steps++
            }
            return steps
        }
    }

    data class Node(val value: String)

    override fun runPart2(lines: List<String>) = TODO("Not yet implemented")
}

fun Day8.DesertMap.Companion.of(lines: List<String>): Day8.DesertMap {
    val instructions = lines[0]
    val nodes = lines.drop(2).map { line ->
        val (key, directions) = line.split("=").map { it.trim() }
        val (left, right) = directions
            .replace("(", "")
            .replace(")", "")
            .split(",")
            .map { it.trim() }
        Day8.Node(key) to Pair(Day8.Node(left), Day8.Node(right))
    }.toMap()
    return Day8.DesertMap(instructions, nodes)
}