import java.io.InputStream
import java.util.SortedSet
import java.util.TreeSet

fun main() {
    val day5 = Day5()
    day5.run("day5.txt")?.let {
        println("Day 5 Part 1 : ${it.first.part1}")
        println("Day 5 Part 2 : ${it.first.part2}")
    }
}

class Day5 : Day<String, Long, Long>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>) =
        SingleSeedAlmanac(lines).seeds.mapNotNull { it.location }.min()

    sealed class Almanac {

        protected var parsingMode = ParsingMode.SEEDS

        abstract fun initSeedsFromLine(line: String)

        enum class ParsingMode {
            SEEDS,
            SEED_TO_SOIL,
            SOIL_TO_FERTILIZER,
            FERTILIZER_TO_WATER,
            WATER_TO_LIGHT,
            LIGHT_TO_TEMPERATURE,
            TEMPERATURE_TO_HUMIDITY,
            HUMIDITY_TO_LOCATION,
        }
    }

    class SingleSeedAlmanac(lines: List<String>) : Almanac() {

        val seeds: MutableList<Seed> = mutableListOf()

        init {
            lines.forEach { line ->
                if (line.startsWith("seeds")) {
                    initSeedsFromLine(line)
                } else if (line.startsWith("seed-to-soil")) {
                    parsingMode = ParsingMode.SEED_TO_SOIL
                    this.seeds.forEach { it.soil = it.id }
                } else if (line.startsWith("soil-to-fertilizer")) {
                    parsingMode = ParsingMode.SOIL_TO_FERTILIZER
                    this.seeds.forEach { it.fertilizer = it.soil }
                } else if (line.startsWith("fertilizer-to-water")) {
                    parsingMode = ParsingMode.FERTILIZER_TO_WATER
                    this.seeds.forEach { it.water = it.fertilizer }
                } else if (line.startsWith("water-to-light")) {
                    parsingMode = ParsingMode.WATER_TO_LIGHT
                    this.seeds.forEach { it.light = it.water }
                } else if (line.startsWith("light-to-temperature")) {
                    parsingMode = ParsingMode.LIGHT_TO_TEMPERATURE
                    this.seeds.forEach { it.temperature = it.light }
                } else if (line.startsWith("temperature-to-humidity")) {
                    parsingMode = ParsingMode.TEMPERATURE_TO_HUMIDITY
                    this.seeds.forEach { it.humidity = it.temperature }
                } else if (line.startsWith("humidity-to-location")) {
                    parsingMode = ParsingMode.HUMIDITY_TO_LOCATION
                    this.seeds.forEach { it.location = it.humidity }
                } else {
                    if (line.isNotEmpty()) {
                        when (parsingMode) {
                            ParsingMode.SEED_TO_SOIL -> {
                                val (destination, source, range) = line.split(" ").map { it.trim().toLong() }
                                this.seeds
                                    .filter { it.id >= source && it.id <= (source + range - 1) }
                                    .forEach { it.soil = destination + (it.id - source) }
                            }

                            ParsingMode.SOIL_TO_FERTILIZER -> {
                                val (destination, source, range) = line.split(" ").map { it.trim().toLong() }
                                this.seeds
                                    .filter { it.soil!! >= source && it.soil!! <= (source + range - 1) }
                                    .forEach { it.fertilizer = destination + (it.soil!! - source) }
                            }

                            ParsingMode.FERTILIZER_TO_WATER -> {
                                val (destination, source, range) = line.split(" ").map { it.trim().toLong() }
                                this.seeds
                                    .filter { it.fertilizer!! >= source && it.fertilizer!! <= (source + range - 1) }
                                    .forEach { it.water = destination + (it.fertilizer!! - source) }
                            }

                            ParsingMode.WATER_TO_LIGHT -> {
                                val (destination, source, range) = line.split(" ").map { it.trim().toLong() }
                                this.seeds
                                    .filter { it.water!! >= source && it.water!! <= (source + range - 1) }
                                    .forEach { it.light = destination + (it.water!! - source) }
                            }

                            ParsingMode.LIGHT_TO_TEMPERATURE -> {
                                val (destination, source, range) = line.split(" ").map { it.trim().toLong() }
                                this.seeds
                                    .filter { it.light!! >= source && it.light!! <= (source + range - 1) }
                                    .forEach { it.temperature = destination + (it.light!! - source) }
                            }

                            ParsingMode.TEMPERATURE_TO_HUMIDITY -> {
                                val (destination, source, range) = line.split(" ").map { it.trim().toLong() }
                                this.seeds
                                    .filter { it.temperature!! >= source && it.temperature!! <= (source + range - 1) }
                                    .forEach { it.humidity = destination + (it.temperature!! - source) }
                            }

                            ParsingMode.HUMIDITY_TO_LOCATION -> {
                                val (destination, source, range) = line.split(" ").map { it.trim().toLong() }
                                this.seeds
                                    .filter { it.humidity!! >= source && it.humidity!! <= (source + range - 1) }
                                    .forEach { it.location = destination + (it.humidity!! - source) }
                            }

                            else -> {}
                        }
                    }
                }
            }
        }

        override fun initSeedsFromLine(line: String) {
            line.split(":")
                .last()
                .trim()
                .split(" ")
                .map { it.trim().toLong() }
                .forEach {
                    seeds.add(Seed(it))
                }
        }
    }

    class SeedsRangeAlmanac(lines: List<String>) : Almanac() {

        val dataRanges: SortedSet<DataRange> = sortedSetOf()

        init {
            val currentMapping = mutableMapOf<DataRange, DataRange>()
            lines.forEach { line ->
                if (line.startsWith("seeds")) {
                    initSeedsFromLine(line)
                    dataRanges.forEach { dataRange ->
                        currentMapping[dataRange] = dataRange
                    }
                } else if (line.startsWith("seed-to-soil")) {
                    dataRanges.forEach { dataRange ->
                        currentMapping[dataRange] = dataRange
                    }
                    parsingMode = ParsingMode.SEED_TO_SOIL
                } else if (line.startsWith("soil-to-fertilizer")) {
                    dataRanges.clear()
                    currentMapping.forEach { dataRanges.add(it.value) }
                    currentMapping.clear()
                    dataRanges.forEach { dataRange -> currentMapping[dataRange] = dataRange }
                    parsingMode = ParsingMode.SOIL_TO_FERTILIZER
                } else if (line.startsWith("fertilizer-to-water")) {
                    dataRanges.clear()
                    currentMapping.forEach { dataRanges.add(it.value) }
                    currentMapping.clear()
                    dataRanges.forEach { dataRange -> currentMapping[dataRange] = dataRange }
                    parsingMode = ParsingMode.FERTILIZER_TO_WATER
                } else if (line.startsWith("water-to-light")) {
                    dataRanges.clear()
                    currentMapping.forEach { dataRanges.add(it.value) }
                    currentMapping.clear()
                    dataRanges.forEach { dataRange -> currentMapping[dataRange] = dataRange }
                    parsingMode = ParsingMode.WATER_TO_LIGHT
                } else if (line.startsWith("light-to-temperature")) {
                    dataRanges.clear()
                    currentMapping.forEach { dataRanges.add(it.value) }
                    currentMapping.clear()
                    dataRanges.forEach { dataRange -> currentMapping[dataRange] = dataRange }
                    parsingMode = ParsingMode.LIGHT_TO_TEMPERATURE
                } else if (line.startsWith("temperature-to-humidity")) {
                    dataRanges.clear()
                    currentMapping.forEach { dataRanges.add(it.value) }
                    currentMapping.clear()
                    dataRanges.forEach { dataRange -> currentMapping[dataRange] = dataRange }
                    parsingMode = ParsingMode.TEMPERATURE_TO_HUMIDITY
                } else if (line.startsWith("humidity-to-location")) {
                    dataRanges.clear()
                    currentMapping.forEach { dataRanges.add(it.value) }
                    currentMapping.clear()
                    dataRanges.forEach { dataRange -> currentMapping[dataRange] = dataRange }
                    parsingMode = ParsingMode.HUMIDITY_TO_LOCATION
                } else {
                    if (line.isNotEmpty()) {
                        val (destination, source, range) = line.split(" ")
                            .map { it.trim().toLong() }
                        val newTempStack = mutableMapOf<DataRange, DataRange>()
                        currentMapping.forEach { entry ->

                            var appliedRange = entry.key.applyMapRule(destination, source, range)

                            if (entry.key != entry.value && appliedRange.size == 1 && appliedRange.first() == entry.key) {
                                appliedRange = listOf(entry.value)
                            }

                            var currentIdx = 0L

                            appliedRange.forEach { currentRange ->
                                val lengthToRetain = currentRange.end - currentRange.start + 1
                                val newKey = DataRange(
                                    entry.key.start + currentIdx,
                                    entry.key.start + currentIdx + lengthToRetain - 1
                                )
                                newTempStack[newKey] = currentRange
                                currentIdx += lengthToRetain
                            }
                        }
                        currentMapping.clear()
                        newTempStack.forEach { currentMapping[it.key] = it.value }
                    }
                }
            }
            dataRanges.clear()
            currentMapping.forEach { dataRanges.add(it.value) }
        }

        override fun initSeedsFromLine(line: String) {
            line.split(":")
                .last()
                .trim()
                .split(" ")
                .map { it.trim().toLong() }
                .chunked(2)
                .forEach {
                    dataRanges.add(DataRange(it[0], it[0] + it[1] - 1))
                }
        }

        data class DataRange(val start: Long, val end: Long) : Comparable<DataRange> {

            init {
                if (start > end) {
                    throw IllegalStateException("$start is greater than $end")
                }
            }

            fun applyMapRule(destination: Long, source: Long, range: Long): List<DataRange> {
                val mapIsBefore = source + range - 1 < start
                val mapIsAfter = source > end
                val result = if (mapIsBefore || mapIsAfter) {
                    listOf(this)
                } else {
                    val rangeDelta = destination - source
                    if (source <= start) {
                        if (source + range - 1 >= end) {
                            listOf(DataRange(start + rangeDelta, end + rangeDelta))
                        } else {
                            if (source == start) {
                                listOf(DataRange(destination, destination + range - 1))
                            } else {
                                val newStart = start + rangeDelta
                                val rangeToApplyAtStart = range - (start - source)
                                val firstRange = DataRange(newStart, newStart + rangeToApplyAtStart - 1)
                                val secondRange = DataRange(start + rangeToApplyAtStart, end)
                                listOf(firstRange, secondRange)
                            }
                        }
                    } else {
                        if (source + range - 1 > end) {
                            val firstRange = DataRange(start, source - 1)
                            val secondRange = if (source > destination) {
                                if (end > start + range) {
                                    val diffToApply = end - source
                                    DataRange(destination, destination + diffToApply)
                                } else {
                                    DataRange(destination, destination + end - source)
                                }
                            } else {
                                DataRange(destination, end + rangeDelta)
                            }
                            listOf(firstRange, secondRange)
                        } else if (source + range - 1 == end) {
                            val firstRange = DataRange(start, source - 1)
                            val secondRange = DataRange(destination, destination + range - 1)
                            listOf(firstRange, secondRange)
                        } else {
                            val firstRange = DataRange(start, source - 1)
                            val secondRange = DataRange(destination, destination + range - 1)
                            val thirdRange = DataRange(source + range, end)
                            listOf(firstRange, secondRange, thirdRange)
                        }
                    }
                }


                return result
            }

            override fun compareTo(other: DataRange) =
                if (this.start == other.start) {
                    this.end.compareTo(other.end)
                } else {
                    this.start.compareTo(other.start)
                }
        }
    }

    data class Seed(
        val id: Long,
        var soil: Long? = null,
        var fertilizer: Long? = null,
        var water: Long? = null,
        var light: Long? = null,
        var temperature: Long? = null,
        var humidity: Long? = null,
        var location: Long? = null,
    )

    override fun runPart2(lines: List<String>) =
        SeedsRangeAlmanac(lines).dataRanges.map { it.start }.min()
}
