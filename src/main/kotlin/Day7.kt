import java.io.InputStream
import java.util.SortedSet
import java.util.TreeSet

fun main() {
    val day7 = Day7()
    day7.run("day7.txt")?.let {
        println("Day 7 Part 1 : ${it.first.part1}")
        println("Day 7 Part 2 : ${it.first.part2}")
    }
}

class Day7 : Day<String, Int, Int>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>) =
        lines
            .map { ClassicalHand.of(it) }
            .toSortedSet()
            .mapIndexed { index, hand -> (index + 1) * hand.bid }
            .sum()

    sealed class Hand(open val value: String, open val bid: Int) : Comparable<Hand> {

        lateinit var type: HandType

        open lateinit var cardList: String

        override fun compareTo(other: Hand): Int {
            if (this.type == other.type) {
                val chars = this.value.map { cardList.indexOf(it) }
                val otherChars = other.value.map { cardList.indexOf(it) }
                val comparisonMap = chars.zip(otherChars)
                return comparisonMap.firstOrNull { it.first != it.second }?.let {
                    return it.second.compareTo(it.first)
                } ?: 0
            } else {
                return this.type.compareTo(other.type)
            }
        }
    }

    class ClassicalHand(override val value: String, override val bid: Int) : Hand(value, bid) {

        override var cardList = "AKQJT98765432"

        companion object {}

        init {
            val charCount = value.groupingBy { it }.eachCount()
            type = if (charCount.values.contains(5)) {
                HandType.FIVE_OF_A_KIND
            } else if (charCount.values.contains(4)) {
                HandType.FOUR_OF_A_KIND
            } else if (charCount.values.contains(3) && charCount.values.contains(2)) {
                HandType.FULL_HOUSE
            } else if (charCount.values.contains(3)) {
                HandType.THREE_OF_A_KIND
            } else if (charCount.values.filter { it == 2 }.size == 2) {
                HandType.TWO_PAIR
            } else if (charCount.values.contains(2)) {
                HandType.ONE_PAIR
            } else {
                HandType.HIGH_CARD
            }
        }
    }

    class HandWithJokerAsWildCard(override val value: String, override val bid: Int) : Hand(value, bid) {

        override var cardList = "AKQT98765432J"

        companion object {}

        init {
            val charCount = value.groupingBy { it }.eachCount()
            val alteredCharCount = if (value.contains("J")) {
                val nbJokers = charCount['J'] ?: throw IllegalStateException("J must be present")
                if(nbJokers == 5){
                    mapOf(cardList[0] to 5)
                } else {
                    val mostRecurringChar = charCount.filter { it.key != 'J' }.maxBy { it.value }
                    charCount - 'J' + Pair(mostRecurringChar.key, mostRecurringChar.value + nbJokers)
                }
            } else {
                charCount
            }
            type = if (alteredCharCount.values.contains(5)) {
                HandType.FIVE_OF_A_KIND
            } else if (alteredCharCount.values.contains(4)) {
                HandType.FOUR_OF_A_KIND
            } else if (alteredCharCount.values.contains(3) && alteredCharCount.values.contains(2)) {
                HandType.FULL_HOUSE
            } else if (alteredCharCount.values.contains(3)) {
                HandType.THREE_OF_A_KIND
            } else if (alteredCharCount.values.filter { it == 2 }.size == 2) {
                HandType.TWO_PAIR
            } else if (alteredCharCount.values.contains(2)) {
                HandType.ONE_PAIR
            } else {
                HandType.HIGH_CARD
            }
        }
    }

    enum class HandType {
        HIGH_CARD,
        ONE_PAIR,
        TWO_PAIR,
        THREE_OF_A_KIND,
        FULL_HOUSE,
        FOUR_OF_A_KIND,
        FIVE_OF_A_KIND
    }

    override fun runPart2(lines: List<String>) =
        lines
            .map { HandWithJokerAsWildCard.of(it) }
            .toSortedSet()
            .mapIndexed { index, hand -> (index + 1) * hand.bid }
            .sum()
}

fun Day7.ClassicalHand.Companion.of(line: String): Day7.Hand {
    val data = line.split(" ")
    return Day7.ClassicalHand(data[0], data[1].toInt())
}

fun Day7.HandWithJokerAsWildCard.Companion.of(line: String): Day7.Hand {
    val data = line.split(" ")
    return Day7.HandWithJokerAsWildCard(data[0], data[1].toInt())
}