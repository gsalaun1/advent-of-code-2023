import java.io.InputStream

fun main() {
    val day2 = Day2()
    day2.run("day2.txt")?.let {
        println("Day 2 Part 1 : ${it.first.part1}")
        println("Day 2 Part 2 : ${it.first.part2}")
    }
}

class Day2 : Day<String, Int, Int>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>): Int {
        val maxCubes = CubesCollection(12, 13, 14)
        return lines
            .map {
                Game(it)
            }.filterNot {
                it.cubesCollectionDoesExceed(maxCubes)
            }.sumOf { it.id }
    }

    override fun runPart2(lines: List<String>): Int =
        lines.map { Game(it) }.sumOf { it.power }

    data class CubesCollection(val red: Int, val green: Int, val blue: Int)

    class Game(value: String) {

        val id: Int
        val maximumRequiredCubesToMatch: CubesCollection
        val power: Int

        init {
            val (gameId, sets) = value.split(":")
            id = gameId.split(" ").last().toInt()
            val maxCubesSet = buildMaxCubesSet(sets)
            maximumRequiredCubesToMatch =
                CubesCollection(maxCubesSet["red"] ?: 0, maxCubesSet["green"] ?: 0, maxCubesSet["blue"] ?: 0)
            power = (maxCubesSet["red"] ?: 1) * (maxCubesSet["green"] ?: 1) * (maxCubesSet["blue"] ?: 1)
        }

        private fun buildMaxCubesSet(sets: String): Map<String, Int> {
            val maxCubes = mutableMapOf("red" to 0, "green" to 0, "blue" to 0)
            val colorsSets = sets.split(";")
            colorsSets.forEach { colorsSet ->
                val colors = colorsSet.split(",").map { it.trim() }
                colors.forEach { color ->
                    val (number, id) = color.split(" ")
                    if (number.toInt() > (maxCubes[id] ?: 0)) {
                        maxCubes[id] = number.toInt()
                    }
                }
            }
            return maxCubes
        }

        fun cubesCollectionDoesExceed(cubesCollection: CubesCollection) =
            maximumRequiredCubesToMatch.blue > cubesCollection.blue ||
                maximumRequiredCubesToMatch.red > cubesCollection.red ||
                maximumRequiredCubesToMatch.green > cubesCollection.green
    }
}
