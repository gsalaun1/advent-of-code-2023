import java.io.InputStream
import java.util.SortedSet
import java.util.TreeSet

fun main() {
    val day6 = Day6()
    day6.run("day6.txt")?.let {
        println("Day 6 Part 1 : ${it.first.part1}")
        println("Day 6 Part 2 : ${it.first.part2}")
    }
}

class Day6 : Day<String, Long, Long>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>): Long =
        Races(lines)
            .races
            .map { it.numberOfWaysToBeatRecord }
            .reduce { acc, ways -> acc * ways }

    class Races(lines: List<String>) {
        val races: List<Race>

        init {
            val times =
                lines[0]
                    .split(":")[1]
                    .split(" ")
                    .filter { it.isNotEmpty() }
                    .map { it.toLong() }
            val distances =
                lines[1]
                    .split(":")[1]
                    .split(" ")
                    .filter { it.isNotEmpty() }
                    .map { it.toLong() }
            races = times.zip(distances).map { Race(it.first, it.second) }
        }
    }

    data class Race(val time: Long, val distance: Long) {
        val numberOfWaysToBeatRecord: Long

        companion object {}

        init {
            numberOfWaysToBeatRecord =
                (0..time)
                    .mapIndexed { index, value ->
                        index * (time - index)
                    }
                    .count { it > distance }
                    .toLong()
        }
    }

    override fun runPart2(lines: List<String>) =
        Race.of(lines).numberOfWaysToBeatRecord
}

fun Day6.Race.Companion.of(lines: List<String>): Day6.Race {
    val time =
        lines[0]
            .split(":")[1]
            .replace(" ", "")
            .toLong()
    val distance =
        lines[1]
            .split(":")[1]
            .replace(" ", "")
            .toLong()
    return Day6.Race(time, distance)
}