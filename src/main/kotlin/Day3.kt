import java.io.InputStream

fun main() {
    val day3 = Day3()
    day3.run("day3.txt")?.let {
        println("Day 3 Part 1 : ${it.first.part1}")
        println("Day 3 Part 2 : ${it.first.part2}")
    }
}

class Day3 : Day<String, Int, Int>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>): Int {
        val numbers = lines.flatMapIndexed { idx, line -> extractNumbers(line, idx) }
        val symbolsLocation = lines.flatMapIndexed { idx, line -> extractSymbolsLocation(line, idx) }
        return numbers.filter { it.isAdjacentToOneOf(symbolsLocation) }.map { it.value }.sum()
    }

    fun extractNumbers(line: String, lineIndex: Int): List<LocatedNumber> {
        val currentNumber = StringBuilder()
        var currentStartIndex = -1
        val locatedNumbers = mutableListOf<LocatedNumber>()
        line.forEachIndexed { idx, char ->
            val currentDigit = char.toString().toIntOrNull()
            if (currentDigit != null) {
                if (currentNumber.isEmpty()) {
                    currentStartIndex = idx
                }
                currentNumber.append(currentDigit)
            } else {
                if (currentNumber.isNotEmpty()) {
                    locatedNumbers.add(
                        LocatedNumber(
                            currentNumber.toString().toInt(),
                            currentStartIndex,
                            idx - 1,
                            lineIndex,
                        ),
                    )
                    currentNumber.clear()
                }
            }
        }
        if (currentNumber.isNotEmpty()) {
            locatedNumbers.add(
                LocatedNumber(
                    currentNumber.toString().toInt(),
                    currentStartIndex,
                    line.length - 1,
                    lineIndex,
                ),
            )
        }
        return locatedNumbers.toList()
    }

    fun extractSymbolsLocation(line: String, lineIndex: Int): List<Location> =
        line.mapIndexedNotNull { idx, char ->
            if (char.toString().toIntOrNull() == null && char != '.') {
                Location(idx, lineIndex)
            } else {
                null
            }
        }

    data class LocatedNumber(
        val value: Int,
        val startingX: Int,
        val endingX: Int,
        val y: Int,
    ) {
        fun isAdjacentToOneOf(symbolsLocation: List<Location>) = symbolsLocation.any { this.isAdjacentTo(it) }

        fun isAdjacentTo(location: Location) =
            location.y >= y - 1 &&
                location.y <= y + 1 &&
                location.x >= startingX - 1 &&
                location.x <= endingX + 1
    }

    data class Location(val x: Int, val y: Int) {
        fun numberNeighbours(numbers: List<LocatedNumber>) =
            numbers.filter { it.isAdjacentTo(this) }
    }

    override fun runPart2(lines: List<String>): Int {
        val numbers = lines.flatMapIndexed { idx, line -> extractNumbers(line, idx) }
        val gearsLocation = lines.flatMapIndexed { idx, line -> extractGearsLocation(line, idx) }
        return gearsLocation.map { it.numberNeighbours(numbers) }.filter { it.size == 2 }
            .sumOf { it[0].value * it[1].value }
    }

    fun extractGearsLocation(line: String, lineIndex: Int) =
        line.mapIndexedNotNull { idx, char ->
            if (char == '*') {
                Location(idx, lineIndex)
            } else {
                null
            }
        }
}
